<?php
/**
 * @file
 * feature_science_olympiad_registration.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function feature_science_olympiad_registration_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create field_registration_price'.
  $permissions['create field_registration_price'] = array(
    'name' => 'create field_registration_price',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_registration_price'.
  $permissions['edit field_registration_price'] = array(
    'name' => 'edit field_registration_price',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_registration_price'.
  $permissions['edit own field_registration_price'] = array(
    'name' => 'edit own field_registration_price',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_registration_price'.
  $permissions['view field_registration_price'] = array(
    'name' => 'view field_registration_price',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_registration_price'.
  $permissions['view own field_registration_price'] = array(
    'name' => 'view own field_registration_price',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  return $permissions;
}
