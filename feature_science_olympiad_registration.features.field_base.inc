<?php
/**
 * @file
 * feature_science_olympiad_registration.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function feature_science_olympiad_registration_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_coach'.
  $field_bases['field_coach'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_coach',
    'indexes' => array(
      'target_id' => array(
        0 => 'target_id',
      ),
    ),
    'locked' => 0,
    'module' => 'entityreference',
    'settings' => array(
      'handler' => 'base',
      'handler_settings' => array(
        'behaviors' => array(
          'views-select-list' => array(
            'status' => 0,
          ),
        ),
        'sort' => array(
          'type' => 'none',
        ),
        'target_bundles' => array(),
      ),
      'target_type' => 'user',
    ),
    'translatable' => 0,
    'type' => 'entityreference',
  );

  // Exported field_base: 'field_district'.
  $field_bases['field_district'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_district',
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(
      'max_length' => 255,
    ),
    'translatable' => 0,
    'type' => 'text',
  );

  // Exported field_base: 'field_division'.
  $field_bases['field_division'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_division',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'b' => 'B',
        'c' => 'C',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_division_b'.
  $field_bases['field_division_b'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_division_b',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'anatomy' => 'Anatomy',
        'bottle_rocket' => 'Bottle Rocket',
        'crime_busters' => 'Crime Busters',
        'disease_detectives' => 'Disease Detectives',
        'dynamic_planet' => 'Dynamic Planet',
        'ecology' => 'Ecology',
        'experimental_design' => 'Experimental Design',
        'fast_facts' => 'Fast Facts',
        'food_science' => 'Food Science',
        'hovercraft' => 'Hovercraft',
        'invasive_species' => 'Invasive Species',
        'meteorology' => 'Meteorology',
        'microbe_mission' => 'Microbe Mission',
        'mission_possible' => 'Mission Possible',
        'optics' => 'Optics',
        'reach_for_stars' => 'Reach for Stars',
        'road_scholar' => 'Road Scholar',
        'rocks_minerals' => 'Rocks & Minerals',
        'scrambler' => 'Scrambler',
        'tower' => 'Tower',
        'wind_power' => 'Wind Power',
        'wright_stuff' => 'Wright Stuff',
        'write_it_do_it' => 'Write It/Do It',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_division_c'.
  $field_bases['field_division_c'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_division_c',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'anatomy_physiology' => 'Anatomy & Physiology',
        'astronomy' => 'Astronomy',
        'chem_lab' => 'Chem Lab',
        'disease_detectives' => 'Disease Detectives',
        'dynamic_planet' => 'Dynamic Planet',
        'ecology' => 'Ecology',
        'electric_vehicle' => 'Electric Vehicle',
        'experimental_design' => 'Experimental Design',
        'forensics' => 'Forensics',
        'game_on' => 'Game On',
        'helicopters' => 'Helicopters',
        'hovercraft' => 'Hovercraft',
        'hydrogeology' => 'Hydrogeology',
        'invasive_species' => 'Invasive Species',
        'material_science' => 'Material Science',
        'microbe_mission' => 'Microbe Mission',
        'optics' => 'Optics',
        'remote_sensing' => 'Remote Sensing',
        'robot_arm' => 'Robot Arm',
        'rocks_minerals' => 'Rocks & Minerals',
        'tower' => 'Tower',
        'wind_power' => 'Wind Power',
        'write_it_do_it' => 'Write It/Do It',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_elementary_supervisors'.
  $field_bases['field_elementary_supervisors'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_elementary_supervisors',
    'indexes' => array(
      'revision_id' => array(
        0 => 'revision_id',
      ),
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'field_collection',
    'settings' => array(
      'hide_blank_items' => 1,
      'hide_initial_item' => 0,
      'path' => '',
    ),
    'translatable' => 0,
    'type' => 'field_collection',
  );

  // Exported field_base: 'field_event_to_supervise_elem'.
  $field_bases['field_event_to_supervise_elem'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_event_to_supervise_elem',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'aerodynamic' => 'Aerodynamics',
        'balloon_race' => 'Balloon Race',
        'barge_building' => 'Barge Building',
        'birds' => 'Birds',
        'can_race' => 'Can Race',
        'crime_busters' => 'Crime Busters',
        'egg_drop' => 'Egg Drop',
        'estimania' => 'Estimania',
        'grasp_graph' => 'Grasp a Graph',
        'junkyard_challenge' => 'Junkyard Challenge',
        'keep_heat' => 'Keep the Heat',
        'mysteria_architecture' => 'Mystery Architecture',
        'paddle_boat_contstruct' => 'Paddle Boat Construction',
        'pasta_bridge' => 'Pasta Bridge',
        'straw_tower' => 'Straw Tower',
        'tennis_ball_catapult' => 'Tennis Ball Catapult',
        'water_rockets' => 'Water Rockets',
        'write_it_do_it' => 'Write it/Do it',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_number_of_team'.
  $field_bases['field_number_of_team'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_number_of_team',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        1 => 1,
        2 => 2,
        3 => 3,
        4 => 4,
        5 => 5,
        6 => 6,
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_integer',
  );

  // Exported field_base: 'field_registration_meals'.
  $field_bases['field_registration_meals'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_registration_meals',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'number',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'number_integer',
  );

  // Exported field_base: 'field_registration_price'.
  $field_bases['field_registration_price'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_registration_price',
    'field_permissions' => array(
      'type' => 2,
    ),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'number',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'number_integer',
  );

  // Exported field_base: 'field_registration_total_due'.
  $field_bases['field_registration_total_due'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_registration_total_due',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'computed_field',
    'settings' => array(
      'code' => '$field_registration_price = field_get_items($entity_type, $entity, "field_registration_price");
$field_number_of_teams = field_get_items($entity_type, $entity, "field_number_of_team");
$entity_field[0][\'value\'] = $field_registration_price[0]["value"] * $field_number_of_teams[0]["value"];',
      'database' => array(
        'data_default' => '',
        'data_index' => 0,
        'data_length' => 32,
        'data_not_NULL' => 0,
        'data_precision' => 10,
        'data_scale' => 2,
        'data_size' => 'normal',
        'data_type' => 'varchar',
      ),
      'display_format' => '$display_output = "$" .$entity_field_item[\'value\'];',
      'recalculate' => 0,
      'store' => 1,
    ),
    'translatable' => 0,
    'type' => 'computed',
  );

  // Exported field_base: 'field_school'.
  $field_bases['field_school'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_school',
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(
      'max_length' => 255,
    ),
    'translatable' => 0,
    'type' => 'text',
  );

  // Exported field_base: 'field_school_level'.
  $field_bases['field_school_level'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_school_level',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'elementary' => 'Elementary',
        'secondary' => 'Secondary',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_secondary_supervisors'.
  $field_bases['field_secondary_supervisors'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_secondary_supervisors',
    'indexes' => array(
      'revision_id' => array(
        0 => 'revision_id',
      ),
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'field_collection',
    'settings' => array(
      'hide_blank_items' => 1,
      'hide_initial_item' => 0,
      'path' => '',
    ),
    'translatable' => 0,
    'type' => 'field_collection',
  );

  // Exported field_base: 'field_supervisor_first_name'.
  $field_bases['field_supervisor_first_name'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_supervisor_first_name',
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(
      'max_length' => 255,
    ),
    'translatable' => 0,
    'type' => 'text',
  );

  // Exported field_base: 'field_supervisor_last_name'.
  $field_bases['field_supervisor_last_name'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_supervisor_last_name',
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(
      'max_length' => 255,
    ),
    'translatable' => 0,
    'type' => 'text',
  );

  // Exported field_base: 'field_supervisor_phone'.
  $field_bases['field_supervisor_phone'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_supervisor_phone',
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(
      'max_length' => 255,
    ),
    'translatable' => 0,
    'type' => 'text',
  );

  return $field_bases;
}
