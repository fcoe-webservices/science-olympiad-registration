<?php
/**
 * @file
 * feature_science_olympiad_registration.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function feature_science_olympiad_registration_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_division_b|field_collection_item|field_secondary_supervisors|form';
  $field_group->group_name = 'group_division_b';
  $field_group->entity_type = 'field_collection_item';
  $field_group->bundle = 'field_secondary_supervisors';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_supervisor_events';
  $field_group->data = array(
    'label' => 'Division B',
    'weight' => '5',
    'children' => array(
      0 => 'field_division_b',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Division B',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-division-b field-group-div columns large-6',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_division_b|field_collection_item|field_secondary_supervisors|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_division_c|field_collection_item|field_secondary_supervisors|form';
  $field_group->group_name = 'group_division_c';
  $field_group->entity_type = 'field_collection_item';
  $field_group->bundle = 'field_secondary_supervisors';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_supervisor_events';
  $field_group->data = array(
    'label' => 'Division C',
    'weight' => '6',
    'children' => array(
      0 => 'field_division_c',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Division C',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-division-c field-group-div columns large-6',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_division_c|field_collection_item|field_secondary_supervisors|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_supervisor_events|field_collection_item|field_elementary_supervisors|form';
  $field_group->group_name = 'group_supervisor_events';
  $field_group->entity_type = 'field_collection_item';
  $field_group->bundle = 'field_elementary_supervisors';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Supervisor Events',
    'weight' => '7',
    'children' => array(
      0 => 'field_event_to_supervise_elem',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Supervisor Events',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-supervisor-events field-group-div columns large-6',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_supervisor_events|field_collection_item|field_elementary_supervisors|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_supervisor_events|field_collection_item|field_secondary_supervisors|form';
  $field_group->group_name = 'group_supervisor_events';
  $field_group->entity_type = 'field_collection_item';
  $field_group->bundle = 'field_secondary_supervisors';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Supervisor Events',
    'weight' => '2',
    'children' => array(
      0 => 'group_division_b',
      1 => 'group_division_c',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Supervisor Events',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-supervisor-events field-group-div ',
        'description' => '',
        'show_label' => '1',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_supervisor_events|field_collection_item|field_secondary_supervisors|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_supervisor_info|field_collection_item|field_elementary_supervisors|form';
  $field_group->group_name = 'group_supervisor_info';
  $field_group->entity_type = 'field_collection_item';
  $field_group->bundle = 'field_elementary_supervisors';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Supervisor Info',
    'weight' => '0',
    'children' => array(
      0 => 'field_supervisor_first_name',
      1 => 'field_supervisor_last_name',
      2 => 'field_supervisor_phone',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Supervisor Info',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-supervisor-info field-group-div columns large-6',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_supervisor_info|field_collection_item|field_elementary_supervisors|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_supervisor_info|field_collection_item|field_secondary_supervisors|form';
  $field_group->group_name = 'group_supervisor_info';
  $field_group->entity_type = 'field_collection_item';
  $field_group->bundle = 'field_secondary_supervisors';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Supervisor Info',
    'weight' => '1',
    'children' => array(
      0 => 'field_supervisor_first_name',
      1 => 'field_supervisor_last_name',
      2 => 'field_supervisor_phone',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Supervisor Info',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-supervisor-info field-group-div ',
        'description' => '',
        'show_label' => '1',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_supervisor_info|field_collection_item|field_secondary_supervisors|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Division B');
  t('Division C');
  t('Supervisor Events');
  t('Supervisor Info');

  return $field_groups;
}
