<?php
/**
 * @file
 * feature_science_olympiad_registration.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function feature_science_olympiad_registration_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function feature_science_olympiad_registration_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function feature_science_olympiad_registration_node_info() {
  $items = array(
    'registration' => array(
      'name' => t('Registration'),
      'base' => 'node_content',
      'description' => t('User <em>registration</em> to add a per school registration for Science Olympiad.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
